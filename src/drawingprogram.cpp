#include "../vendor/glad/glad.h"
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <Magick++.h>

#include "../vendor/shader.h"
#include "button.h"

#include <iostream>
#include <exception>
#include <cmath>
#include <stack>

#define COLOR_BUTTONS_AMOUNT 10
#define TOOL_BUTTONS_AMOUNT 4

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void processInput(GLFWwindow *window);
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
void mouse_button_callback(GLFWwindow* window, int button, int action, int mods);

// settings
const float PADDING = 0.1f;
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 800;
const unsigned int DRAWING_WIDTH = 700;

bool flood_fill_mode = false;
GLFWcursor* pencilCursor;
GLFWcursor* fillCursor;

double previousCursorX, previousCursorY;
int previousMouseButton;

typedef struct Drawing {
    int width;
    int height;
    unsigned char* data;
    unsigned char active_color[4];
    int brush_size = 1;
    Magick::Image* brush_image;
    unsigned char* scaled_brush;
    Drawing() {
        width = 0;
        height = 0;
        data = NULL;
        scaled_brush = NULL;
        brush_image = NULL;
        setActiveColor(0,0,0,255);
    }
    Drawing(int width, int height, unsigned char* data) {
        this->width = width;
        this->height = height;
        this->data = data;
        scaled_brush = NULL;
        brush_image = NULL;
        setActiveColor(0,0,0,255);
    }
    int xOnCanvas(double x) {
        double xWithoutPadding = x - SCR_WIDTH*(PADDING/2);
        double canvasWidth = SCR_WIDTH-SCR_WIDTH*PADDING;
        double xNormalizedOnCanvas = xWithoutPadding/canvasWidth;
        return xNormalizedOnCanvas*width;
    }
    int yOnCanvas(double y) {
        double yWithoutPadding = y - SCR_HEIGHT*(PADDING/2);
        double canvasHeight = SCR_HEIGHT-SCR_HEIGHT*PADDING;
        double yNormalizedOnCanvas = yWithoutPadding/canvasHeight;
        return yNormalizedOnCanvas*height;
    }
    void drawAt(int x, int y) {
        if (data == NULL) return;
        if (x < 0 || y < 0 || x > width-1 || y > height-1) return;
        if (brush_image == NULL || scaled_brush == NULL) {
            setPixel(x,y,active_color[0],active_color[1],active_color[2],active_color[3]);
            return;
        }
        int x2,y2;
        for (int i = 0; i < brush_size; i++) {
            for (int j = 0; j < brush_size; j++) {
                if (scaled_brush[i*brush_size*4 + j*4 + 3] == 0) continue;
                x2 = x - brush_size/2 + i;
                y2 = y - brush_size/2 + j;
                setPixel(x2,y2,
                         active_color[0],active_color[1],active_color[2],1);
            }
        }
    }
    void drawLine(double previousX, double previousY, double x, double y) {
        int lineLength = sqrt(pow(x-previousX,2) + pow(y-previousY,2));
        int xInLine, yInLine;
        for (int i = 0; i < lineLength; i++) {
            xInLine = floor(previousX + (x-previousX)*i/lineLength);
            yInLine = floor(previousY + (y-previousY)*i/lineLength);
            drawAt(xInLine, yInLine);
        }
    }
    void floodFill(int x, int y) {
        if (data == NULL) return;
        if (x < 0 || y < 0 || x > width-1 || y > height-1) return;
        unsigned char oldColor[4] = {
            data[y*width*4 + x*4 + 0],
            data[y*width*4 + x*4 + 1],
            data[y*width*4 + x*4 + 2],
            data[y*width*4 + x*4 + 3]
        };
        if (pixelIsColor(x, y, active_color)) return;
        //floodFill(x, y, oldColor, active_color);
        floodFill2(x,y);
    }
    bool pixelIsColor(int x, int y, unsigned char* color) {
        return (data[y*width*4 + x*4 + 0] == color[0] &&
                data[y*width*4 + x*4 + 1] == color[1] &&
                data[y*width*4 + x*4 + 2] == color[2] &&
                data[y*width*4 + x*4 + 3] == color[3]);
    }
    bool outOfBounds(int x, int y) {
        return (x < 0 || y < 0 || x > width-1 || y > height-1);
    }
    void setPixel(int x, int y, unsigned char r, unsigned char g, 
                                unsigned char b, unsigned char a) {
        if (outOfBounds(x,y)) return;
        data[y*width*4 + x*4 + 0] = r;
        data[y*width*4 + x*4 + 1] = g;
        data[y*width*4 + x*4 + 2] = b;
        data[y*width*4 + x*4 + 3] = a;
    }
    void setActiveColor(unsigned char r, unsigned char g, unsigned char b, unsigned char a) {
        active_color[0] = r;
        active_color[1] = g;
        active_color[2] = b;
        active_color[3] = a;
    }
    void setActiveColor(unsigned char* color) {
        active_color[0] = color[0];
        active_color[1] = color[1];
        active_color[2] = color[2];
        active_color[3] = color[3];
    }
    unsigned char getR(int x, int y) {
        return data[y*width*4 + x*4 + 0];
    }
    unsigned char getG(int x, int y) {
        return data[y*width*4 + x*4 + 1];
    }
    unsigned char getB(int x, int y) {
        return data[y*width*4 + x*4 + 2];
    }
    unsigned char getA(int x, int y) {
        return data[y*width*4 + x*4 + 3];
    }
    // old bad floodfill function that causes stack overflow
    void floodFill(int x, int y, unsigned char* oldColor, unsigned char* newColor) {
        if (oldColor == NULL) {
            printf("old null\n");
            return;
        }
        if (newColor == NULL) {
            printf("new null\n");
            return;
        }
        setPixel(x,y,newColor[0],newColor[1],newColor[2],newColor[3]);
        if (x+1 < width && pixelIsColor(x+1,y,oldColor)) {
            floodFill(x+1,y,oldColor,newColor);
        }
        if (y+1 < height && pixelIsColor(x,y+1,oldColor)) {
            floodFill(x,y+1,oldColor,newColor);
        }
        if (x-1 >= 0 && pixelIsColor(x-1,y,oldColor)) {
            floodFill(x-1,y,oldColor,newColor);
        }
        if (y-1 >= 0 && pixelIsColor(x,y-1,oldColor)) {
            floodFill(x,y-1,oldColor,newColor);
        }
    }
    // new good floodfill function
    void floodFill2(int x, int y) {
        struct SnapShotStruct {
            int x;
            int y;
            int stage;
        };
        unsigned char oldColor[4] = {
            getR(x,y),
            getG(x,y),
            getB(x,y),
            getA(x,y)
        };
        std::stack<SnapShotStruct> snapshotStack;

        SnapShotStruct currentSnapshot;
        currentSnapshot.x = x;
        currentSnapshot.y = y;
        currentSnapshot.stage = 0;
        
        snapshotStack.push(currentSnapshot);

        while (!snapshotStack.empty()) {
            currentSnapshot = snapshotStack.top();
            snapshotStack.pop();
            switch(currentSnapshot.stage) {
                case 0:
                    setPixel(currentSnapshot.x,currentSnapshot.y,
                             active_color[0],active_color[1],
                             active_color[2],active_color[3]);
                    currentSnapshot.stage = 1;
                    snapshotStack.push(currentSnapshot);
                    continue;
                    break;
                case 1:
                    currentSnapshot.stage = 2;
                    snapshotStack.push(currentSnapshot);
                    if (currentSnapshot.x+1 < width && pixelIsColor(currentSnapshot.x+1,
                                                                    currentSnapshot.y,
                                                                    oldColor)) {
                        SnapShotStruct newSnapshot;
                        newSnapshot.x = currentSnapshot.x+1;
                        newSnapshot.y = currentSnapshot.y;
                        newSnapshot.stage = 0;
                        snapshotStack.push(newSnapshot);
                    }
                    continue;
                    break;
                case 2:
                    currentSnapshot.stage = 3;
                    snapshotStack.push(currentSnapshot);
                    if (currentSnapshot.y+1 < height && pixelIsColor(currentSnapshot.x,
                                                     currentSnapshot.y+1,
                                                     oldColor)) {
                        SnapShotStruct newSnapshot;
                        newSnapshot.x = currentSnapshot.x;
                        newSnapshot.y = currentSnapshot.y+1;  
                        newSnapshot.stage = 0;
                        snapshotStack.push(newSnapshot);
                    }
                    continue;
                    break;
                case 3:
                    currentSnapshot.stage = 4;
                    snapshotStack.push(currentSnapshot);
                    if (currentSnapshot.x-1 >= 0 && pixelIsColor(currentSnapshot.x-1,
                                                                 currentSnapshot.y,
                                                                 oldColor)) {
                        SnapShotStruct newSnapshot;
                        newSnapshot.x = currentSnapshot.x-1;  
                        newSnapshot.y = currentSnapshot.y;
                        newSnapshot.stage = 0;
                        snapshotStack.push(newSnapshot);
                    }
                    continue;
                    break;
                case 4:
                    if (currentSnapshot.y-1 >= 0 && pixelIsColor(currentSnapshot.x,
                                                                 currentSnapshot.y-1,
                                                                 oldColor)) {
                        SnapShotStruct newSnapshot;
                        newSnapshot.x = currentSnapshot.x;  
                        newSnapshot.y = currentSnapshot.y-1;
                        newSnapshot.stage = 0;
                        snapshotStack.push(newSnapshot);
                    }
                    continue;
                    break;
            }
        }
    }
    void increaseBrushSize() {
        if (brush_size == 50) return;
        brush_size += 1;
        scaleBrush();
    }
    void decreaseBrushSize() {
        if (brush_size == 1) return;
        brush_size -= 1;
        scaleBrush();
    }
    void scaleBrush() {
        Magick::Image brush = *brush_image;
        std::string s = std::to_string(brush_size);
        brush.resize(Magick::Geometry(s + "x" + s));
        if (scaled_brush != NULL) free(scaled_brush);
        scaled_brush = (unsigned char*) malloc(brush_size*brush_size*4*sizeof(unsigned char));
        if (scaled_brush == 0) {
            printf("memory exceeded\n");
            exit(1);
        }
        brush.write(0,0,brush_size,brush_size,"RGBA",Magick::CharPixel,scaled_brush);
    }
    void save() {
        if (data == NULL) return;
        try {
            Magick::Image image;
            image.read(width, height, "RGBA", Magick::CharPixel, data);
            image.write("result.png");
        } catch (std::exception &error_) {
            std::cout << "Caught exception: " << error_.what() << std::endl;
        }
    }
} Drawing;

Drawing drawing;
ColorButton colorButtons[COLOR_BUTTONS_AMOUNT];
ToolButton toolButtons[TOOL_BUTTONS_AMOUNT];

unsigned char data[DRAWING_WIDTH][(int) DRAWING_WIDTH*SCR_HEIGHT/SCR_WIDTH][4];

int main(int argc, char **argv)
{
    Magick::InitializeMagick(*argv);
    
    colorButtons[0].setColor(0,0,0,255);
    colorButtons[1].setColor(255,255,255,255);
    colorButtons[2].setColor(255,0,0,255);
    colorButtons[3].setColor(0,255,0,255);
    colorButtons[4].setColor(0,0,255,255);
    colorButtons[5].setColor(255,170,200,255);
    colorButtons[6].setColor(255,255,0,255);
    colorButtons[7].setColor(127,58,0,255);
    colorButtons[8].setColor(220,0,255,255);
    colorButtons[9].setColor(255,100,0,255);

    int width, height;
    width = DRAWING_WIDTH;
    height = (int) DRAWING_WIDTH*SCR_HEIGHT/SCR_WIDTH;
    for (int i = 0; i < width; i++) {
        for (int j = 0; j < height; j++) {
            data[i][j][0] = 255;
            data[i][j][1] = 255;
            data[i][j][2] = 255;
            data[i][j][3] = 255;
        }
    }

    drawing.width = width;
    drawing.height = height;
    drawing.data = (unsigned char*)data;

    Magick::Image brushImg;
    brushImg.read("res/images/circle.png");
    drawing.brush_image = &brushImg;

    // glfw: initialize and configure
    // ------------------------------
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

    // glfw window creation
    // --------------------
    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "drawing program", NULL, NULL);
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    
    glfwSetKeyCallback(window, key_callback);
    glfwSetMouseButtonCallback(window, mouse_button_callback);


    // glad: load all OpenGL function pointers
    // ---------------------------------------
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

    Shader colorButtonShader = Shader("res/cbshader.vs","res/cbshader.fs");
    for (int i = 0; i < COLOR_BUTTONS_AMOUNT; i++) {
        colorButtons[i].init(&colorButtonShader, i, SCR_WIDTH, SCR_HEIGHT);
    }

    Shader toolButtonShader = Shader("res/tbshader.vs","res/tbshader.fs");
    toolButtons[0].init(&toolButtonShader, "res/images/fill.png", 0, SCR_WIDTH, SCR_HEIGHT);
    toolButtons[1].init(&toolButtonShader, "res/images/pencil.png", 1, SCR_WIDTH, SCR_HEIGHT);
    toolButtons[2].init(&toolButtonShader, "res/images/pencilplus.png", 2, SCR_WIDTH, SCR_HEIGHT);
    toolButtons[3].init(&toolButtonShader, "res/images/pencilminus.png", 3, SCR_WIDTH, SCR_HEIGHT);
    
    
    Shader ourShader("res/shader.vs", "res/shader.fs");

    // canvas vertices
    float vertices[] = {
    // positions                   // texture coords
     1-PADDING,  1-PADDING, 0.0f,  1.0f, 1.0f,   // top right
     1-PADDING, -1+PADDING, 0.0f,  1.0f, 0.0f,   // bottom right
    -1+PADDING, -1+PADDING, 0.0f,  0.0f, 0.0f,   // bottom left
    -1+PADDING,  1-PADDING, 0.0f,  0.0f, 1.0f    // top left 
    };

    // CURSOR STUFF

    unsigned char cursorPixels[32*32*4];

    Magick::Image cursorImage;
    cursorImage.read("res/images/fillcursor.png");
    cursorImage.write(0,0,32,32,"RGBA",Magick::CharPixel,cursorPixels);

    GLFWimage c;
    c.width = 32;
    c.height = 32;
    c.pixels = cursorPixels;

    fillCursor = glfwCreateCursor(&c,0,8);
    if (fillCursor == NULL) {
        printf("cursor creation failed\n");
        exit(1);
    }

    cursorImage.read("res/images/pencilcursor.png");
    cursorImage.write(0,0,32,32,"RGBA",Magick::CharPixel,cursorPixels);

    c.pixels = cursorPixels;

    pencilCursor = glfwCreateCursor(&c,0,17);
    if (pencilCursor == NULL) {
        printf("cursor creation failed\n");
        exit(1);
    }

    glfwSetCursor(window, pencilCursor);
    
    // TEXTURE STUFF
    

    unsigned int texture;
    glGenTextures(1, &texture);

    glBindTexture(GL_TEXTURE_2D, texture);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);



    unsigned int VBO, VAO;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    // bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s).
    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    // position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);

    unsigned int indices[] = {  
        0, 1, 3, // first triangle
        1, 2, 3  // second triangle
    };

    unsigned int EBO;
    glGenBuffers(1, &EBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);


    ourShader.use();
    ourShader.setInt("texture1", 0);

    // render loop
    // -----------
    while (!glfwWindowShouldClose(window))
    {
        // input
        // -----
        processInput(window);

        // render
        // ------
        glClearColor(1.0f, 0.75f, 0.8f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);


        
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture);

        ourShader.use(); // don't forget to activate the shader before setting uniforms!  



        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);

        glBindVertexArray(VAO);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

        for (int i = 0; i < COLOR_BUTTONS_AMOUNT; i++) {
            colorButtons[i].draw();
        }
        for (int i = 0; i < TOOL_BUTTONS_AMOUNT; i++) {
            toolButtons[i].draw(glm::vec4(((float)drawing.active_color[0]/255),
                                          ((float)drawing.active_color[1]/255),
                                          ((float)drawing.active_color[2]/255),
                                          ((float)drawing.active_color[3]/255)));
        }
        // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
        // -------------------------------------------------------------------------------
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    // optional: de-allocate all resources once they've outlived their purpose:
    // ------------------------------------------------------------------------
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);

    // glfw: terminate, clearing all previously allocated GLFW resources.
    // ------------------------------------------------------------------
    glfwTerminate();
    return 0;
}

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// --------------------------------------------------------------------------------------------
void processInput(GLFWwindow *window)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        free(drawing.scaled_brush);
        glfwSetWindowShouldClose(window, true);
    }
    int state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
    if (state == GLFW_PRESS)
    {
        double xpos, ypos;
        glfwGetCursorPos(window, &xpos, &ypos);
        int xOnCanvas = drawing.xOnCanvas(xpos);
        int yOnCanvas = drawing.yOnCanvas(ypos);
        if (!flood_fill_mode) {
            drawing.drawAt(xOnCanvas, yOnCanvas);
            
            if (previousMouseButton == GLFW_PRESS) {
                drawing.drawLine(drawing.xOnCanvas(previousCursorX), 
                                 drawing.yOnCanvas(previousCursorY),
                                 xOnCanvas, yOnCanvas);
            }
        }
        previousCursorX = xpos;
        previousCursorY = ypos;
    }
    previousMouseButton = state;
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_S && action == GLFW_RELEASE)
        drawing.save();
    else if (key == GLFW_KEY_F && action == GLFW_RELEASE)
        flood_fill_mode = !flood_fill_mode;
    else if (key == GLFW_KEY_COMMA && action == GLFW_RELEASE)
        drawing.decreaseBrushSize();
    else if (key == GLFW_KEY_PERIOD && action == GLFW_RELEASE)
        drawing.increaseBrushSize();
}
void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
    double xpos, ypos;
    glfwGetCursorPos(window, &xpos, &ypos);
    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
        int xOnCanvas = drawing.xOnCanvas(xpos);
        int yOnCanvas = drawing.yOnCanvas(ypos);
        if (flood_fill_mode)
            drawing.floodFill(xOnCanvas,yOnCanvas);
        if (xpos < colorButtons[COLOR_BUTTONS_AMOUNT-1].x + colorButtons[4].widthPx 
                    && ypos < colorButtons[4].heightPx
                    && (int)floor(xpos/colorButtons[4].widthPx) < COLOR_BUTTONS_AMOUNT) {
                unsigned char* newColor = (unsigned char*)colorButtons[(int)floor(xpos/colorButtons[4].widthPx)].bgColor;
                drawing.setActiveColor(newColor);
        } else {
            if (toolButtons[0].hover(xpos,ypos)) {
                flood_fill_mode = true;
                glfwSetCursor(window,fillCursor);
            } else if (toolButtons[1].hover(xpos,ypos)) {
                flood_fill_mode = false;
                glfwSetCursor(window,pencilCursor);
            } else if (toolButtons[2].hover(xpos,ypos)) {
                drawing.increaseBrushSize();
            } else if (toolButtons[3].hover(xpos,ypos)) {
                drawing.decreaseBrushSize();
            }
        }
    }
}
// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and 
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}
