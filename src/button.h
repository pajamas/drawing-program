#ifndef BUTTON_H_DEFINED
#define BUTTON_H_DEFINED

class Button {
    public:
    static constexpr unsigned int indices[6] = {
        0, 1, 3, // first triangle
        0, 2, 3  // second triangle
    };
    static constexpr float width = 0.06f;
    Shader* shader;
    float vertices[12];
    unsigned char bgColor[4];
    float x,y,widthPx,heightPx;
    unsigned int VAO, VBO, EBO;
    Button() {
        bgColor[0] = 127;
        bgColor[1] = 127;
        bgColor[2] = 127;
        bgColor[3] = 255;
    }
    bool hover(double x, double y) {
        return (x > this->x && x < this->x + widthPx
                && y > this->y && y < this->y + heightPx);
    }
    void draw() {
        if (shader == NULL) {
            printf("shader was null\n");
            return;
        }
        shader->use();
        shader->setVec4("color", glm::vec4((bgColor[0])/255.0f,(bgColor[1])/255.0f,
                                           (bgColor[1])/255.0f,(bgColor[1])/255.0f));
        glBindVertexArray(VAO);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
    }
};

class ColorButton: public Button {
    public:
    ColorButton() {
        setColor(127,127,127,255);
    }
    void init(Shader* shader, int buttonNum, int SCR_WIDTH, int SCR_HEIGHT) {
        this->shader = shader;

        auto init = std::initializer_list<float>({
            -1+buttonNum*width, 1.0f, 0.0f,
            -1+(buttonNum+1)*width, 1.0f, 0.0f,
            -1+buttonNum*width, 1-width, 0.0f,
            -1+(buttonNum+1)*width, 1-width, 0.0f
        });
        std::copy(init.begin(), init.end(), vertices);

        x = buttonNum*SCR_WIDTH*width;
        y = 0;
        widthPx = width*SCR_WIDTH/2;
        heightPx = width*SCR_HEIGHT/2;
        
        glGenVertexArrays(1, &VAO);
        glGenBuffers(1, &VBO);
        glBindVertexArray(VAO);
       
        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

        // position attribute
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
        glEnableVertexAttribArray(0); 

        glGenBuffers(1, &EBO);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
    }
    void setColor(unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha) {
        bgColor[0] = red;
        bgColor[1] = green;
        bgColor[2] = blue;
        bgColor[3] = alpha;
    }
    void draw() {
        if (shader == NULL) {
            printf("shader was null\n");
            return;
        }
        shader->use();
        shader->setVec4("color", glm::vec4((float)bgColor[0]/255,(float)bgColor[1]/255,
                                           (float)bgColor[2]/255,(float)bgColor[3]/255));
        glBindVertexArray(VAO);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
    }
};

class ToolButton: public Button {
    public:
    int buttonNum;
    unsigned int texture;
    unsigned char data[32][32][4];
    float vertices[20];
    static constexpr float width = 0.08f;
    ToolButton() : Button() {
    }
    void init(Shader* shader, const std::string imagePath, int buttonNum,
              int SCR_WIDTH, int SCR_HEIGHT) {  
        Magick::Image image;
        image.read(imagePath);
        image.write(0,0,32,32,"RGBA",Magick::CharPixel,data);

        this->buttonNum = buttonNum;
        this->shader = shader;
        this->shader->use();
        this->shader->setInt("buttonImage",1);
        this->shader->setInt("activeColor",1);

        glGenTextures(1, &texture);

        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, texture);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 32, 32, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);

        auto init = std::initializer_list<float>({
            1-buttonNum*width, -1.0f, 0.0f, 1.0f, 0.0f, // bottom right
            1-(buttonNum+1)*width, -1.0f, 0.0f, 0.0f, 0.0f,  // bottom left
            1-buttonNum*width, -1+width, 0.0f, 1.0f, 1.0f,// top right
            1-(buttonNum+1)*width, -1+width, 0.0f, 0.0f, 1.0f // top left
        });
        std::copy(init.begin(), init.end(), vertices);

        widthPx = width*SCR_WIDTH/2;
        heightPx = width*SCR_HEIGHT/2;
        x = SCR_WIDTH-(buttonNum+1)*widthPx;
        y = SCR_HEIGHT-heightPx;
        
        glGenVertexArrays(1, &VAO);
        glGenBuffers(1, &VBO);
        glBindVertexArray(VAO);

        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

        // position attribute
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
        glEnableVertexAttribArray(0); 
        // texture coords
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), 
                              (void*)(3*sizeof(float)));
        glEnableVertexAttribArray(1); 

        glGenBuffers(1, &EBO);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
    }
    void draw(glm::vec4 activeColor) {
        if (shader == NULL) {
            printf("shader was null\n");
            return;
        }
        shader->use();
        shader->setVec4("color", glm::vec4((float)bgColor[0]/255,(float)bgColor[1]/255,
                                           (float)bgColor[2]/255,(float)bgColor[3]/255));
        shader->setVec4("activeColor", activeColor);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, texture);
        glBindVertexArray(VAO);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
    }
};

#endif
