#version 330 core
out vec4 FragColor;

in vec2 TexCoord;

uniform vec4 color;
uniform vec4 activeColor;
uniform sampler2D buttonImage;

void main()
{
    vec4 tex = texture(buttonImage, TexCoord);
    FragColor = vec4((color*(1-tex.a)+tex*tex.a).rgb,1);
    if (tex == vec4(0,0,1,1)) {
        FragColor = activeColor;
    }
    if (TexCoord.x < 0.03 || TexCoord.x > 0.97 || TexCoord.y < 0.03 || TexCoord.y > 0.97) {
        FragColor += 0.3;
    }
}
