# Drawing program

f to toggle flood fill mode

s to save

. to increase brush size

, to decrease brush size

![drawing](res/images/mermaid.png)

install dependencies (Ubuntu):

```
sudo apt install libglfw3-dev
sudo apt install graphicsmagick-libmagick-dev-compat
```

